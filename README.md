# DARTS: Differentiable Architecture Search (Tensorflow)

Tensorflow 2.0+ implementation for convolutional neural networks cells architecture search.
> [DARTS: Differentiable Architecture Search](https://arxiv.org/abs/1806.09055)\
> Hanxiao Liu, Karen Simonyan, Yiming Yang.\
> _arXiv:1806.09055_.


## Requirements
```
Python >= 3.7, tensorflow-gpu >= 2.10.0, tensorflow-addons >= 0.18.0, networkx >= 2.8.7, matplotlib >= 3.6.1, pygraphviz >= 1.10
```
NOTE: Probably all of the above Tensorflow 2.4.0 versions will work fine.


## Dataset
The dataset for training and searching for cell architecture can be downloaded [here](https://www.cs.toronto.edu/~kriz/cifar.html) (select CIFAR-10 python version).

After downloading CIFAR-10 data, unpack it to any directory - `DATA_PATH`.


## Creating TFRecords 

**STEP 1 - SETTING `DATA_PATH`**

If you want to create tfrecords files from CIFAR-10 data, just enter `DATA_PATH` from the previous step.

It is possible to create tfrecords files from custom images (`.jpg`). To do this, in place of `DATA_PATH` enter the path to the directory with images.

**STEP 2 - SETTING `TFRECORDS_PATH`**

Select the directory where you want to store the tfrecords files - `TFRECORDS_PATH`.

**STEP 3 - RUN `build_tfrecords.py`**

Run the script with the selected flags as follows:
```
python darts-tensorflow/src/cnn/build_tfrecords.py --data_path=DATA_PATH --tfrecords_path=TFRECORDS_PATH
```

## Architecture search

**STEP 1 - SETTING `TFRECORDS_PATH`**

Select the directory where you store the tfrecords files - `TFRECORDS_PATH`.

**STEP 2 - SETTING `RESULTS_PATH`**

Select the directory where you want to store results e.g. checkpoints, logs, profiler and tensorboard outputs - `RESULTS_PATH`.

**STEP 3 - RUN `search_model.py`**

Run the script with the selected flags as follows:
```
python darts-tensorflow/src/cnn/search_model.py --tfrecords_path=TFRECORDS_PATH --results_path=RESULTS_PATH
```

**EXTRA FLAGS** (for `search_model.py` script)

It is possible to use more flags to personalize the architecture search. All flags are described in the `darts-tensorflow/src/cnn/utils/flags.py`.

## Train generated architecture

**STEP 1 - SETTING `TFRECORDS_PATH`**

Select the directory where you store the tfrecords files - `TFRECORDS_PATH`.

**STEP 2 - SETTING `RESULTS_PATH`**

Select the directory where you want to store results e.g. checkpoints, logs, tensorboard outputs - `RESULTS_PATH`.

**STEP 3 - SETTING `GENOTYPE`**

Set a text value for the generated cell genotype. After the architecture search stage, you will find it in Tensorboard. The genotype format must match this implementer. 

Example:
```
"Genotype(normal=[('conv_7x1_1x7', 0), ('dil_conv_3x3', 1), ('conv_7x1_1x7', 0), ('sep_conv_7x7', 2), ('dil_conv_5x5', 1), ('sep_conv_5x5', 0), ('sep_conv_7x7', 4), ('skip_connect', 0)], normal_concat=range(2, 6), reduce=[('conv_7x1_1x7', 0), ('dil_conv_3x3', 1), ('sep_conv_3x3', 1), ('conv_7x1_1x7', 2), ('skip_connect', 0), ('dil_conv_3x3', 2), ('dil_conv_5x5', 4), ('dil_conv_3x3', 2)],  reduce_concat=range(2, 6))"
```
*Above genotype is random, it does not lead to correct results.*

**STEP 4 - RUN `train_model.py`**

Run the script with the selected flags as follows:
```
python darts-tensorflow/src/cnn/train_model.py --tfrecords_path=TFRECORDS_PATH --results_path=RESULTS_PATH --genotype=GENOTYPE
```

**EXTRA FLAGS** (for `train_model.py` script)

It is possible to use more flags to personalize the architecture training. All flags are described in the `darts-tensorflow/src/cnn/utils/flags.py`.

