import tensorflow as tf
import tensorflow_addons as tfa

from absl import app

from src.cnn.utils.flags import *
from src.cnn.utils.network import Network
from src.cnn.utils.architect import Architect
from src.cnn.utils.dataset.consts import EVAL
from src.cnn.utils.dataset.dataset_reader import ImageDatasetReader

FLAGS = flags.FLAGS
tf.get_logger().setLevel("INFO")

physical_devices = tf.config.list_physical_devices('GPU')
if len(physical_devices) > 0:
    for device in physical_devices:
        tf.config.experimental.set_memory_growth(device, True)


def search(_):
    # ToDo: Some code refactor in Network, Architect and search_model script.
    loss_fn = tf.keras.losses.CategoricalCrossentropy(from_logits=True)
    optimizer = tfa.optimizers.SGDW(
        learning_rate=FLAGS.learning_rate,
        momentum=FLAGS.momentum,
        weight_decay=FLAGS.weight_decay
    )
    architecture_optimizer = tfa.optimizers.AdamW(
        learning_rate=3e-4,
        beta_1=0.5,
        beta_2=0.999,
        weight_decay=1e-3
    )
    train_data_reader = ImageDatasetReader(
        batch_size=FLAGS.batch_size,
        epochs=FLAGS.epochs,
        tfrecords_path=FLAGS.tfrecords_path,
        num_classes=FLAGS.num_classes
    )
    eval_data_reader = ImageDatasetReader(
        batch_size=FLAGS.batch_size,
        epochs=FLAGS.epochs,
        tfrecords_path=FLAGS.tfrecords_path,
        mode=EVAL,
        num_classes=FLAGS.num_classes
    )
    network = Network(
        num_filters=FLAGS.num_filters,
        num_filters_multiplier=FLAGS.num_filters_multiplier,
        num_layers=FLAGS.num_layers,
        num_nodes=FLAGS.num_nodes,
        num_reduction_nodes=FLAGS.num_reduction_nodes,
        num_classes=FLAGS.num_classes,
        input_shape=(32, 32, 3)
    )
    architecture = Architect(
        batch_size=FLAGS.batch_size,
        num_epochs=FLAGS.epochs,
        network=network,
        architecture_optimizer=architecture_optimizer,
        optimizer=optimizer,
        loss_fn=loss_fn,
        start_epoch_search=1,
        unrolled=True,
        results_path=FLAGS.results_path,
        learning_rate=FLAGS.learning_rate,
        min_learning_rate=FLAGS.min_learning_rate,
        profiler=False
    )
    architecture.search(
        train_dataset_reader=train_data_reader,
        validation_dataset_reader=eval_data_reader
    )


if __name__ == '__main__':
    app.run(search)
