import os

TFRECORDS_PATH = os.getenv('TFRECORDS_PATH')
DATA_PATH = os.getenv('DATA_PATH')
BUILD_RESULTS_PATH = os.getenv('BUILD_RESULTS_PATH')
TRAIN = "train"
EVAL = "validation"


class TFRecordFields:
    height = 'image/height'
    width = 'image/width'
    channels = 'image/channels'
    classes = 'image/classes'
    label = 'image/label'
    filename = 'image/filename'
    source_id = 'image/source_id'
    image = 'image/encoded'
