import tensorflow as tf

from src.cnn.utils.operations.consts import PRIMITIVES
from src.cnn.utils.operations.modules import OPERATIONS_MAP_2D


class MixedOp(tf.keras.layers.Layer):
    def __init__(self, num_filters, stride, name=None, **kwargs):
        super(MixedOp, self).__init__(name=name, **kwargs)
        self.ops = None
        self.stride = stride
        self.num_filters = num_filters

        self._init_ops()

    def _init_ops(self):
        self.ops = []
        for primitive in PRIMITIVES:
            self.ops.append(OPERATIONS_MAP_2D[primitive](
                num_filters=self.num_filters,
                stride=self.stride,
                name=self.name + '_' + str(primitive)
            ))

    def call(self, inputs, *args, **kwargs):
        x, alphas_weights = inputs
        return tf.add_n([w * op(x) for w, op in zip(tf.split(alphas_weights, len(PRIMITIVES)), self.ops)])
