from collections import namedtuple

PRIMITIVES = [
    'none',
    'skip_connect',
    'max_pool_3x3',
    'avg_pool_3x3',
    'dil_conv_3x3',
    'dil_conv_5x5',
    'conv_7x1_1x7',
    'sep_conv_3x3',
    'sep_conv_5x5',
    'sep_conv_7x7'
]

Genotype = namedtuple('Genotype', 'normal normal_concat reduce reduce_concat')
