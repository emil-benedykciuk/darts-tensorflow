import tensorflow as tf

OPERATIONS_MAP_2D = {
    'none': lambda num_filters, stride, name:
        ZeroModule(stride, name=name),
    'skip_connect': lambda num_filters, stride, name:
        IdentityModule(name=name) if stride == 1 else FactorizedReduceModule(num_filters, name=name),
    'max_pool_3x3': lambda num_filters, stride, name:
        tf.keras.layers.MaxPooling2D(pool_size=(3, 3), strides=stride, padding='same', name=name + "_max_pool"),
    'avg_pool_3x3': lambda num_filters, stride, name:
        tf.keras.layers.AveragePooling2D(pool_size=(3, 3), strides=stride, padding='same', name=name + "_avg_pool_1"),
    'conv_3x3': lambda num_filters, stride, name:
        Convolution2D(num_filters, 3, stride, name=name),
    'conv_5x5': lambda num_filters, stride, name:
        Convolution2D(num_filters, 5, stride, name=name),
    'dil_conv_3x3': lambda num_filters, stride, name:
        DilationConvolution2D(num_filters, 3, stride, 2, name=name),
    'dil_conv_5x5': lambda num_filters, stride, name:
        DilationConvolution2D(num_filters, 5, stride, 2, name=name),
    'sep_conv_3x3': lambda num_filters, stride, name:
        SeparableConvolutional2D(num_filters, 3, stride, name=name),
    'sep_conv_5x5': lambda num_filters, stride, name:
        SeparableConvolutional2D(num_filters, 5, stride, name=name),
    'sep_conv_7x7': lambda num_filters, stride, name:
        SeparableConvolutional2D(num_filters, 7, stride, name=name),
    'conv_7x1_1x7': lambda num_filters, stride, name:
        AsymmetricConvolution2D7x7(num_filters, stride, name=name),
}


class ZeroModule(tf.keras.layers.Layer):
    def __init__(self, stride, **kwargs):
        super(ZeroModule, self).__init__(**kwargs)
        self.stride = stride

    def call(self, inputs, *args, **kwargs):
        if self.stride == 1:
            return tf.multiply(inputs, 0.)
        return tf.multiply(inputs[:, ::self.stride, ::self.stride, :], 0.)


class IdentityModule(tf.keras.layers.Layer):
    def __init__(self, **kwargs):
        super(IdentityModule, self).__init__(**kwargs)

    def call(self, inputs, *args, **kwargs):
        return inputs


class FactorizedReduceModule(tf.keras.layers.Layer):
    def __init__(self, num_filters, affine=True, name="factorized_reduce_module", **kwargs):
        super(FactorizedReduceModule, self).__init__(name=name, **kwargs)
        self.num_filters = num_filters // 2
        self.kernel_size = (1, 1)
        self.stride = (2, 2)
        self.padding = "valid"
        self.use_bias = False
        self.affine = affine

        self._init_ops()

    def _init_ops(self):
        self._relu = tf.keras.layers.ReLU(
            name=self.name + "_relu"
        )
        self._conv_1 = tf.keras.layers.Conv2D(
            filters=self.num_filters,
            kernel_size=self.kernel_size,
            strides=self.stride,
            padding=self.padding,
            use_bias=self.use_bias,
            name=self.name + "_conv_1"
        )
        self._conv_2 = tf.keras.layers.Conv2D(
            filters=self.num_filters,
            kernel_size=self.kernel_size,
            strides=self.stride,
            padding=self.padding,
            use_bias=self.use_bias,
            name=self.name + "_conv_2"
        )
        self._normalization = tf.keras.layers.BatchNormalization(
            scale=self.affine,
            name=self.name + "_bn"
        )

    def call(self, inputs, *args, **kwargs):
        inputs = self._relu(inputs)
        inputs = tf.concat([self._conv_1(inputs), self._conv_2(inputs[:, 1:, 1:, :])], axis=-1)
        inputs = self._normalization(inputs)
        return inputs


class SeparableConvolutional2D(tf.keras.layers.Layer):
    def __init__(self, num_filters, kernel_size, stride, affine=True, name="separable_conv", **kwargs):
        super(SeparableConvolutional2D, self).__init__(name=name, **kwargs)
        self.num_filters = num_filters
        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = 'same'
        self.use_bias = False
        self.affine = affine

        self._init_ops()

    def _init_ops(self):
        self._ops = tf.keras.Sequential([
            tf.keras.layers.ReLU(
                name=self.name + "_1_relu"
            ),
            tf.keras.layers.SeparableConv2D(
                filters=self.num_filters,
                kernel_size=self.kernel_size,
                strides=self.stride,
                padding=self.padding,
                use_bias=self.use_bias,
                name=self.name + "_1_sep_conv_1"
            ),
            tf.keras.layers.BatchNormalization(
                scale=self.affine,
                name=self.name + "_1_bn"
            ),
            tf.keras.layers.ReLU(
                name=self.name + "_2_relu"
            ),
            tf.keras.layers.SeparableConv2D(
                filters=self.num_filters,
                kernel_size=self.kernel_size,
                strides=1,
                padding=self.padding,
                use_bias=self.use_bias,
                name=self.name + "_2_sep_conv_1"
            ),
            tf.keras.layers.BatchNormalization(
                scale=self.affine,
                name=self.name + "_2_bn"
            )
        ])

    def call(self, inputs, *args, **kwargs):
        inputs = self._ops(inputs)
        return inputs


class AsymmetricConvolution2D7x7(tf.keras.layers.Layer):
    """
    Conv: 7x1 - 1x7
    """
    def __init__(self, num_filters, stride, affine=True, padding='same', name="conv_7117", **kwargs):
        super(AsymmetricConvolution2D7x7, self).__init__(name=name, **kwargs)
        self.num_filters = num_filters
        self.kernel_size = 7
        self.stride = stride
        self.use_bias = False
        self.affine = affine
        self.padding = padding

        self._init_ops()

    def _init_ops(self):
        self._ops = tf.keras.Sequential([
            tf.keras.layers.ReLU(
                name=self.name + "_relu"
            ),
            tf.keras.layers.Conv2D(
                filters=self.num_filters,
                kernel_size=(1, self.kernel_size),
                strides=(1, self.stride),
                padding=self.padding,
                use_bias=self.use_bias,
                name=self.name + "_conv_1x7"
            ),
            tf.keras.layers.Conv2D(
                filters=self.num_filters,
                kernel_size=(self.kernel_size, 1),
                strides=(self.stride, 1),
                padding=self.padding,
                use_bias=self.use_bias,
                name=self.name + "_conv_7x1"
            ),
            tf.keras.layers.BatchNormalization(
                scale=self.affine,
                name=self.name + '_bn'
            )
        ])

    def call(self, inputs, *args, **kwargs):
        inputs = self._ops(inputs)
        return inputs


class DilationConvolution2D(tf.keras.layers.Layer):
    def __init__(self, num_filters, kernel_size, stride, dilation_rate, padding='same', affine=True, name="conv",
                 **kwargs):
        super(DilationConvolution2D, self).__init__(name=name, **kwargs)
        self.num_filters = num_filters
        self.kernel_size = kernel_size
        self.stride = stride
        self.use_bias = False
        self.dilation_rate = dilation_rate
        self.padding = padding
        self.affine = affine

        self._init_ops()

    def _init_ops(self):
        self._ops = tf.keras.Sequential([
            tf.keras.layers.ReLU(
                name=self.name + "_relu"
            ),
            tf.keras.layers.SeparableConv2D(
                filters=self.num_filters,
                kernel_size=self.kernel_size,
                strides=self.stride,
                dilation_rate=self.dilation_rate,
                padding=self.padding,
                use_bias=self.use_bias,
                name=self.name + "_1_sep_conv_1"
            ),
            tf.keras.layers.BatchNormalization(
                scale=self.affine,
                name=self.name + "_1_bn"
            ),
        ])

    def call(self, inputs, *args, **kwargs):
        inputs = self._ops(inputs)
        return inputs


class Convolution2D(tf.keras.layers.Layer):
    def __init__(self, num_filters, kernel_size, stride, affine=True, activation_first=True, padding='same',
                 name="conv", **kwargs):
        super(Convolution2D, self).__init__(name=name, **kwargs)
        self.num_filters = num_filters
        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding
        self.use_bias = False
        self.affine = affine
        self.activation_first = activation_first

        self._init_ops()

    def _init_ops(self):
        self._relu_activation = tf.keras.layers.ReLU(
            name=self.name + "_relu"
        )
        self._conv_1 = tf.keras.layers.Conv2D(
            filters=self.num_filters,
            kernel_size=(self.kernel_size, self.kernel_size),
            strides=(self.stride, self.stride),
            padding=self.padding,
            use_bias=self.use_bias,
            name=self.name + "_conv_1"
        )
        self._normalization = tf.keras.layers.BatchNormalization(
            scale=self.affine,
            name=self.name + "_bn"
        )

        _ops_list = []
        if self.activation_first:
            _ops_list.append(self._relu_activation)
        _ops_list.append(self._conv_1)
        _ops_list.append(self._normalization)
        if not self.activation_first:
            _ops_list.append(self._relu_activation)
        self._ops = tf.keras.Sequential(_ops_list)

    def call(self, inputs, *args, **kwargs):
        inputs = self._ops(inputs)
        return inputs
