import io
import abc

import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import tensorflow as tf

from PIL import Image

from src.cnn.utils.cell import Cell, GeneratedCell
from src.cnn.utils.operations.consts import PRIMITIVES, Genotype


class BaseNetwork:
    def __init__(self, num_filters, num_filters_multiplier, num_layers, num_classes, input_shape=(32, 32, 3),
                 name="network"):
        self.name = name
        self.num_filters = num_filters
        self.num_filters_multiplier = num_filters_multiplier
        self.num_layers = num_layers
        self.num_classes = num_classes
        self.input_shape = input_shape
        self.cells = None

        self.inputs = tf.keras.layers.Input(self.input_shape, name=self.name + "_input_image")
        self._init_base_ops()
        self._init_cells_ops()
        self.model = self._build_model()

    def _init_base_ops(self):
        self._conv_0 = tf.keras.layers.Conv2D(
            filters=self.num_filters * self.num_filters_multiplier,
            kernel_size=3,
            strides=1,
            padding='same',
            use_bias=False,
            name=self.name + '_conv_1'
        )
        self._bn_0 = tf.keras.layers.BatchNormalization(
            scale=True,
            name=self.name + '_batch'
        )
        self._global_avg_pooling = tf.keras.layers.GlobalAvgPool2D(
            name=self.name + '_global_pool'
        )
        self._logits = tf.keras.layers.Dense(
            units=self.num_classes,
            name=self.name + '_logits'
        )

    @abc.abstractmethod
    def _init_cells_ops(self):
        pass

    @abc.abstractmethod
    def _model(self) -> tf.keras.layers.Layer:
        pass

    @abc.abstractmethod
    def _build_model(self) -> tf.keras.models.Model:
        pass


class GeneratedNetworkCIFAR(BaseNetwork):
    def __init__(self, num_filters, num_filters_multiplier, num_layers, num_classes, input_shape=(32, 32, 3),
                 cells_genotype=None, name="network"):
        assert cells_genotype
        self.cells_genotype = cells_genotype
        super(GeneratedNetworkCIFAR, self).__init__(
            num_filters=num_filters,
            num_filters_multiplier=num_filters_multiplier,
            num_layers=num_layers,
            num_classes=num_classes,
            input_shape=input_shape,
            name=name
        )

    def _init_cells_ops(self):
        _filters = self.num_filters
        _is_cell_reduction = False
        _is_prev_cell_reduction = False
        self.cells = []
        for i in range(self.num_layers):
            if i in [self.num_layers // 3, 2 * self.num_layers // 3]:
                _filters *= 2
                _is_cell_reduction = True
            else:
                _is_cell_reduction = False
            _cell = GeneratedCell(
                num_filters=_filters,
                is_cell_reduction=_is_cell_reduction,
                is_prev_cell_reduction=_is_prev_cell_reduction,
                genotype=self.cells_genotype,
                name=self.name + '_' + str(i) + ('_reduction_cell_op' if _is_cell_reduction else '_normal_cell_op')
            )
            _is_prev_cell_reduction = _is_cell_reduction
            self.cells.append(_cell)

    def _model(self) -> tf.keras.layers.Layer:
        features = self._conv_0(self.inputs)
        features = self._bn_0(features)
        features_0, features_1 = features, features
        for cell in self.cells:
            features_0, features_1 = features_1, cell([features_0, features_1])
        out = self._global_avg_pooling(features_1)
        logits = self._logits(out)
        return logits

    def _build_model(self) -> tf.keras.models.Model:
        return tf.keras.models.Model(self.inputs, self._model())


class Network(BaseNetwork):
    def __init__(self, num_filters, num_filters_multiplier, num_layers, num_nodes, num_reduction_nodes, num_classes,
                 input_shape=(32, 32, 3), name="network", initialize_alphas=True):
        self.num_nodes = num_nodes
        self.num_reduction_nodes = num_reduction_nodes
        self.alphas = None
        self.reduce_cell_alphas = None
        self.normal_cell_alphas = None
        self.num_edges = sum(range(2, self.num_nodes + 2))
        self.alphas_shape = (self.num_edges, len(PRIMITIVES))
        if initialize_alphas:
            self._init_alphas()
        self.normal_alphas_inputs = tf.keras.layers.Input([None], name=name + "_normal_alphas")
        self.reduce_alphas_inputs = tf.keras.layers.Input([None], name=name + "_reduce_alphas")
        self.normal_alphas_weights = tf.keras.layers.Softmax()(self.normal_alphas_inputs)
        self.reduce_alphas_weights = tf.keras.layers.Softmax()(self.reduce_alphas_inputs)
        super(Network, self).__init__(
            num_filters=num_filters,
            num_filters_multiplier=num_filters_multiplier,
            num_layers=num_layers,
            num_classes=num_classes,
            input_shape=input_shape,
            name=name
        )
        self.weights = [v for v in self.model.trainable_variables if 'alphas' not in v.name]

    def _init_cells_ops(self):
        _filters = self.num_filters
        _is_cell_reduction = False
        _is_prev_cell_reduction = False
        self.cells = []
        for i in range(self.num_layers):
            if i in [self.num_layers // 3, 2 * self.num_layers // 3]:
                _filters *= 2
                _is_cell_reduction = True
            else:
                _is_cell_reduction = False
            _alphas_weights = self.reduce_alphas_weights if _is_cell_reduction else self.normal_alphas_weights
            _cell = Cell(
                num_nodes=self.num_nodes,
                num_reduction_nodes=self.num_reduction_nodes,
                num_filters=_filters,
                is_cell_reduction=_is_cell_reduction,
                is_prev_cell_reduction=_is_prev_cell_reduction,
                name=self.name + '_' + str(i) + ('_reduction_cell_op' if _is_cell_reduction else '_normal_cell_op')
            )
            _is_prev_cell_reduction = _is_cell_reduction
            self.cells.append((_cell, _alphas_weights))

    def _init_alphas(self):
        self.normal_cell_alphas = tf.Variable(
            initial_value=tf.random_normal_initializer()(shape=self.alphas_shape, dtype='float32'),
            trainable=True,
            name="normal_alphas"
        )
        self.reduce_cell_alphas = tf.Variable(
            initial_value=tf.random_normal_initializer()(shape=self.alphas_shape, dtype='float32'),
            trainable=True,
            name="reduce_alphas"
        )
        self.alphas = [self.normal_cell_alphas, self.reduce_cell_alphas]

    def _model(self) -> tf.keras.layers.Layer:
        features = self._conv_0(self.inputs)
        features = self._bn_0(features)
        features_0, features_1 = features, features
        for (cell, alphas_weights) in self.cells:
            features_0, features_1 = features_1, cell([features_0, features_1, alphas_weights])
        out = self._global_avg_pooling(features_1)
        logits = self._logits(out)
        return logits

    def _build_model(self) -> tf.keras.models.Model:
        return tf.keras.models.Model(
            (self.inputs, self.normal_alphas_inputs, self.reduce_alphas_inputs),
            self._model()
        )

    def get_genotype(self):
        def _parse(alphas):
            n = 2
            start = 0
            gene = []
            for i in range(self.num_nodes):
                end = start + n
                weights = alphas[start:end].copy()
                edges = sorted(range(i + 2), key=lambda x: -max(
                    weights[x][_k] for _k in range(len(weights[x])) if _k != PRIMITIVES.index('none')))[:2]
                for j in edges:
                    k_best = None
                    for k in range(len(weights[j])):
                        if k != PRIMITIVES.index('none'):
                            if k_best is None or weights[j][k] > weights[j][k_best]:
                                k_best = k
                    gene.append((PRIMITIVES[k_best], j))
                start = end
                n += 1
            return gene

        gene_normal = _parse(tf.nn.softmax(self.normal_cell_alphas).numpy())
        gene_reduce = _parse(tf.nn.softmax(self.reduce_cell_alphas).numpy())

        result = range(self.num_nodes - self.num_reduction_nodes + 2, self.num_nodes + 2)
        return Genotype(
            normal=gene_normal, normal_concat=result,
            reduce=gene_reduce, reduce_concat=result
        )

    def get_genotype_image(self):
        def generate_edges(_nodes, normal_cell=True):
            _node = 2
            _edges = {}
            genotype = self.get_genotype()
            _genotype = genotype.normal if normal_cell else genotype.reduce
            for i in range(0, self.num_nodes * 2, 2):
                _edges[(_genotype[i][1], _node)] = _genotype[i][0]
                _edges[(_genotype[i + 1][1], _node)] = _genotype[i + 1][0]
                _node += 1

            _node_id = _nodes[-2]
            for _ in range(self.num_reduction_nodes):
                _edges[(_node_id, _nodes[-1])] = 'concat'
                _node_id -= 1
            return _edges

        def generate_di_graph(_nodes, _edges):
            _g = nx.DiGraph()
            _g.add_nodes_from(_nodes)
            _g.add_edges_from(_edges)
            return _g

        def draw_di_graph(_g, _labels, _edges):
            _pos = nx.drawing.nx_agraph.graphviz_layout(
                _g,
                prog='dot',
                args='-Gsplines=true -Gnodesep=0.6 -Goverlap=scalexy'
            )
            nx.draw(_g, _pos, labels=_labels, with_labels=True, node_size=1000)
            nx.draw_networkx_edge_labels(_g, _pos, edge_labels=_edges, font_color='red')

        normal_buffer = io.BytesIO()
        reduce_buffer = io.BytesIO()
        nodes = list(range(self.num_nodes + 3))  # +2 - input cell / states, +1 - cell result
        labels = ['cell_(k-2)', 'cell_(k-1)'] + [str(i) for i in range(self.num_nodes)] + ['cell_(k)']
        nodes_map = {k: v for k, v in zip(nodes, labels)}

        # Plot cells
        plt.figure(figsize=(14, 14))

        # Normal Cell
        n_edges = generate_edges(nodes)
        n_g = generate_di_graph(nodes_map, n_edges)
        draw_di_graph(n_g, nodes_map, n_edges)
        plt.axis('off')
        plt.savefig(normal_buffer, format='png')
        plt.clf()

        # Reduce Cell
        r_edges = generate_edges(nodes, False)
        r_g = generate_di_graph(nodes_map, r_edges)
        draw_di_graph(r_g, nodes_map, r_edges)
        plt.axis('off')
        plt.savefig(reduce_buffer, format='png')
        plt.clf()

        return np.array(Image.open(normal_buffer)), np.array(Image.open(reduce_buffer))
