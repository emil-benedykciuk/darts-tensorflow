import sys
import time


class SimpleProgressBar:
    def __init__(self, name="", total=100, step=0, length=100, training=False):
        self._name = name
        self._length = length
        self._total = total
        self._step = step
        self._start_time = time.time()
        self._global_loss = 0.
        self._step_loss = 0.
        self._training = training

    def _print_bar(self):
        blocks = int(round((self._step / self._total) * self._length))
        if self._training:
            text = "\r{0:<20}: [{1}] {2:.2f}% global loss:{3:.4f}, step loss:{4:.4f}".format(
                self._name,
                "#" * blocks + "-" * (self._length - blocks),
                (self._step / self._total) * 100,
                self._global_loss,
                self._step_loss
            )
        else:
            text = "\r{0:<20}: [{1}] {2:.2f}%".format(
                self._name,
                "#" * blocks + "-" * (self._length - blocks),
                (self._step / self._total) * 100
            )
        sys.stdout.write(text)
        sys.stdout.flush()

    def update_losses(self, global_loss, step_loss):
        self._global_loss = global_loss
        self._step_loss = step_loss

    def update_step(self, step):
        self._step = step
        self._print_bar()

    def next(self):
        self._step += 1
        self._print_bar()

    def end(self):
        elapsed_time = time.time() - self._start_time
        hours, rem = divmod(elapsed_time, 3600)
        minutes, seconds = divmod(rem, 60)
        print(" Task completed in: {:0>2}:{:0>2}:{:05.2f}.".format(int(hours), int(minutes), seconds))
