import numpy as np


class AverageMeter(object):
    def __init__(self):
        self.avg = 0
        self.sum = 0
        self.cnt = 0

        self.reset()

    def reset(self):
        self.avg = 0
        self.sum = 0
        self.cnt = 0

    def update(self, val, n=1):
        self.sum += val * n
        self.cnt += n
        self.avg = self.sum / self.cnt


def softmax(x):
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum(axis=0)


def accuracy(outputs, labels, top_k=(1,)):
    max_k = max(top_k)
    batch_size = labels.shape[0]

    pred = np.argsort(softmax(outputs))[:, ::-1][:, :max_k]
    correct = (pred == np.argsort(labels)[:, ::-1][:, :max_k])

    results = []
    for k in top_k:
        correct_k = np.sum(correct[:, :k])
        results.append(correct_k * 100.0 / batch_size)

    return results
