import os
import math

import tensorflow as tf

from src.cnn.utils.metrics import AverageMeter, accuracy
from src.cnn.utils.network import Network
from src.cnn.utils.progress_bar import SimpleProgressBar


class Architect:
    def __init__(self, batch_size, num_epochs, network, architecture_optimizer, optimizer, loss_fn, start_epoch_search,
                 unrolled, results_path, learning_rate, min_learning_rate, profiler=False):
        self.batch_size = batch_size
        self.num_epochs = num_epochs
        self.network = network
        self.unrolled_network = self._create_network_copy()
        self.architecture_optimizer = architecture_optimizer
        self.optimizer = optimizer
        self.loss_fn = loss_fn
        self.start_epoch_search = start_epoch_search
        self.unrolled = unrolled
        self.results_path = results_path
        self.learning_rate = learning_rate
        self.initial_learning_rate = learning_rate
        self.min_learning_rate = min_learning_rate
        self.profiler = profiler

    def _lr_decay(self, epoch, initial_learning_rate, alpha=0.0):
        lr = self._cosine_decay(initial_learning_rate, epoch, self.num_epochs, alpha)
        return lr if lr > self.min_learning_rate else self.min_learning_rate

    @staticmethod
    def _cosine_decay(initial_learning_rate, step, decay_steps, alpha):
        step = min(step, decay_steps)
        cosine_decay = 0.5 * (1 + tf.math.cos(math.pi * step / decay_steps))
        decayed = (1 - alpha) * cosine_decay + alpha
        return initial_learning_rate * decayed

    def _create_network_copy(self):
        network = Network(
            num_filters=self.network.num_filters,
            num_filters_multiplier=self.network.num_filters_multiplier,
            num_layers=self.network.num_layers,
            num_nodes=self.network.num_nodes,
            num_reduction_nodes=self.network.num_reduction_nodes,
            num_classes=self.network.num_classes,
            input_shape=self.network.input_shape,
            name='unrolled_network',
            initialize_alphas=False
        )
        network.normal_cell_alphas = self.network.normal_cell_alphas
        network.reduce_cell_alphas = self.network.reduce_cell_alphas
        network.alphas = self.network.alphas
        network.model.set_weights(self.network.model.get_weights())
        return network

    @tf.function
    def _compute_unrolled_network(self, images, labels):
        with tf.GradientTape() as tape:
            logits = self.unrolled_network.model((images, *self.network.alphas), training=True)
            loss = self.loss_fn(labels, logits)
        grads = tape.gradient(loss, self.unrolled_network.weights)
        del tape
        # w' model calculation
        self.optimizer.apply_gradients(zip(grads, self.unrolled_network.weights))

    @tf.function
    def _compute_unrolled_network_grads(self, images, labels):
        with tf.GradientTape(persistent=True) as tape:
            logits = self.unrolled_network.model((images, *self.network.alphas), training=True)
            model_loss_value = self.loss_fn(labels, logits)

        # equation 8 (needs for epsilon calculation)
        model_grads = tape.gradient(model_loss_value, self.unrolled_network.weights)

        # equation 7 (the first element of expression)
        model_alpha_grads = tape.gradient(model_loss_value, self.unrolled_network.alphas)

        return model_alpha_grads, model_grads

    def step(self, images, labels, val_images, val_labels, unrolled):
        if unrolled:
            self.second_order_approximation_step(images, labels, val_images, val_labels)
        else:
            self.fist_order_approximation_step(val_images, val_labels)

    @tf.function
    def _tf__compute_alpha_grad(self, grad, implicit_grad):
        shape = implicit_grad.shape
        return tf.map_fn(
            lambda t: tf.subtract(t[0], tf.scalar_mul(self.learning_rate, t[1])),
            (tf.reshape(grad, shape), implicit_grad),
            dtype=tf.float32)

    @tf.function
    def fist_order_approximation_step(self, images, labels):
        with tf.GradientTape() as tape:
            logits = self.network.model((images, *self.network.alphas), training=True)
            model_loss_value = self.loss_fn(labels, logits)
        model_grads = tape.gradient(model_loss_value, self.network.alphas)
        del tape
        self.architecture_optimizer.apply_gradients(zip(model_grads, self.network.alphas))

    def second_order_approximation_step(self, images, labels, val_images, val_labels):
        self._compute_unrolled_network(images, labels)
        model_alpha_grads, model_grads = self._compute_unrolled_network_grads(val_images, val_labels)

        # equation 7 (the second element of expression)
        implicit_grads = self.hessian_vector_product(model_grads, images, labels)

        # equation 7 (result gradient)
        alpha_grad = self._tf__compute_alpha_grad(model_alpha_grads, implicit_grads)

        self.architecture_optimizer.apply_gradients(zip(
            alpha_grad,
            self.unrolled_network.alphas))

    @tf.function
    def hessian_vector_product(self, model_grad, images, labels, r=1e-2):
        def _compute_network_grad(img, lbl):
            with tf.GradientTape() as tape:
                logits = self.network.model((img, *self.network.alphas), training=True)
                loss = self.loss_fn(lbl, logits)
            # equation 8 (approximation right element)
            return tape.gradient(loss, self.network.alphas)

        def flatten_grad(grad):
            temp = tf.TensorArray(tf.float32, size=0, dynamic_size=True, infer_shape=False)
            for _g in grad:
                temp = temp.write(temp.size(), tf.reshape(_g, (tf.math.reduce_prod(tf.shape(_g)),)))
            return temp.concat()

        # equation 8 (epsilon)
        epsilon = r / tf.norm(flatten_grad(model_grad))

        # equation 8 (w+)
        for w, g in zip(self.network.weights, model_grad):
            w.assign_add(tf.multiply(epsilon, g))
        grads_positive_epsilon = _compute_network_grad(images, labels)

        # equation 8 (w-)
        for w, g in zip(self.network.weights, model_grad):
            w.assign_sub(tf.multiply(2. * epsilon, g))
        grads_negative_epsilon = _compute_network_grad(images, labels)

        # reset weights
        for w, g in zip(self.network.weights, model_grad):
            w.assign_add(tf.multiply(epsilon, g))

        # equation 8 (approximation value)
        return tf.divide(tf.subtract(grads_positive_epsilon, grads_negative_epsilon), 2. * epsilon)

    def search_step(self, images, labels, val_images, val_labels):
        self.step(images, labels, val_images, val_labels, self.unrolled)
        with tf.GradientTape() as tape:
            logits = self.network.model((images, *self.network.alphas), training=True)
            loss = self.loss_fn(labels, logits)
        grads = tape.gradient(loss, self.network.weights)
        del tape
        self.optimizer.apply_gradients(zip(grads, self.network.weights))
        return logits, loss

    def search(self, train_dataset_reader, validation_dataset_reader):
        ###################################
        # Metric, tensorboard definitions #
        ###################################
        logs_path = os.path.join(self.results_path, 'logs')
        writer = tf.summary.create_file_writer(logs_path)
        checkpoint = tf.train.Checkpoint(step=tf.Variable(0, name='step'),
                                         optimizer=self.optimizer,
                                         optimizer_arch=self.architecture_optimizer,
                                         model=self.network.model,
                                         alphas_normal=self.network.normal_cell_alphas,
                                         alphas_reduce=self.network.reduce_cell_alphas)
        manager = tf.train.CheckpointManager(checkpoint=checkpoint,
                                             directory=os.path.join(self.results_path, 'checkpoints'),
                                             max_to_keep=3)
        train_acc = AverageMeter()

        ###############
        # Search loop #
        ###############
        epoch = 1
        num_steps_per_epoch = len(train_dataset_reader) // self.batch_size
        train_dataset = train_dataset_reader()
        val_dataset = validation_dataset_reader()
        global_loss = 0.
        prog = None
        if self.profiler:
            tf.profiler.experimental.start(logdir=logs_path)
        for step, (images, labels) in enumerate(train_dataset):
            checkpoint.step.assign_add(step)
            with tf.profiler.experimental.Trace('train', step_num=step, _r=1):
                if epoch >= self.start_epoch_search:
                    val_images, val_labels = next(iter(val_dataset))
                    logits, loss = self.search_step(images, labels, val_images, val_labels)

                ####################################
                # Metrics step, update tensorboard #
                ####################################
                train_acc.update(accuracy(logits.numpy(), labels.numpy())[0], self.batch_size)
                global_loss += loss
                if self.profiler:
                    if step == 4:
                        tf.profiler.experimental.stop()
                if not step % 10:
                    with writer.as_default():
                        Architect.step_summary(
                            global_loss=global_loss,
                            step=step,
                            train_acc=train_acc,
                            loss=loss
                        )
                if not step % num_steps_per_epoch:
                    with writer.as_default():
                        normal_cell, reduce_cell = self.network.get_genotype_image()
                        Architect.epoch_summary(
                            global_loss=global_loss,
                            step=step,
                            epoch=epoch,
                            genotype=self.network.get_genotype(),
                            normal_cell=normal_cell,
                            reduce_cell=reduce_cell
                        )
                    if prog:
                        prog.end()
                    prog = SimpleProgressBar(name="Epoch {0}:".format(epoch), total=num_steps_per_epoch, training=True)
                    self.learning_rate = self._lr_decay(
                        epoch=epoch,
                        initial_learning_rate=self.initial_learning_rate
                    )
                    epoch += 1
                    train_acc.reset()
                    manager.save()
            prog.update_losses(global_loss / (step + 1), loss)
            prog.next()
        prog.end()
        manager.save()

    @staticmethod
    def step_summary(global_loss, step, train_acc, loss):
        tf.summary.scalar('darts-search/train', train_acc.avg, step=step)
        tf.summary.scalar('darts-search/loss', loss, step=step)
        tf.summary.scalar('darts-search/global_loss', global_loss / (step + 1), step=step)

    @staticmethod
    def epoch_summary(global_loss, step, epoch, genotype, normal_cell, reduce_cell):
        tf.summary.scalar('darts-search/epoch/global_loss', global_loss / (step + 1), step=epoch)
        tf.summary.text('darts-search/epoch/genotype', str(genotype), step=epoch)

        tf.summary.image('darts-search/epoch/normal_cell', data=normal_cell[tf.newaxis, ...], step=epoch)
        tf.summary.image('darts-search/epoch/reduce_cell', data=reduce_cell[tf.newaxis, ...], step=epoch)
