import abc
import tensorflow as tf

from src.cnn.utils.mixed_operations import MixedOp
from src.cnn.utils.operations.consts import *
from src.cnn.utils.operations.modules import OPERATIONS_MAP_2D, FactorizedReduceModule, Convolution2D


class BaseCell(tf.keras.layers.Layer):
    def __init__(self, num_filters, is_cell_reduction, is_prev_cell_reduction, name, **kwargs):
        super(BaseCell, self).__init__(name=name, **kwargs)
        self.num_filters = num_filters
        self.is_cell_reduction = is_cell_reduction
        self.is_prev_cell_reduction = is_prev_cell_reduction

        self._init_preprocessing_ops()
        self._init_main_ops()

    def _init_preprocessing_ops(self):
        if self.is_prev_cell_reduction:
            self._preprocessing_module_in_0 = FactorizedReduceModule(
                num_filters=self.num_filters,
                affine=False,
                name=self.name + "_factorized"
            )
        else:
            self._preprocessing_module_in_0 = Convolution2D(
                num_filters=self.num_filters,
                kernel_size=1,
                stride=1,
                padding='valid',
                affine=False,
                name=self.name + "_conv_1"
            )
        self._preprocessing_module_in_1 = Convolution2D(
            num_filters=self.num_filters,
            kernel_size=1,
            stride=1,
            padding='valid',
            affine=False,
            name=self.name + "_conv_2"
        )

    @abc.abstractmethod
    def _init_main_ops(self):
        pass


class GeneratedCell(BaseCell):
    def __init__(self, num_filters, is_cell_reduction, is_prev_cell_reduction, genotype, name=None,
                 **kwargs):
        self.genotype = genotype
        super(GeneratedCell, self).__init__(
            num_filters=num_filters,
            is_cell_reduction=is_cell_reduction,
            is_prev_cell_reduction=is_prev_cell_reduction,
            name=name,
            **kwargs)

    def _get_ops_definition(self):
        _genotype = eval(self.genotype)
        if self.is_cell_reduction:
            op_names, indices = zip(*_genotype.reduce)
            concat = _genotype.reduce_concat
        else:
            op_names, indices = zip(*_genotype.normal)
            concat = _genotype.normal_concat
        return op_names, indices, concat

    def _init_main_ops(self):
        op_names, indices, concat = self._get_ops_definition()
        self._ops = []
        self._steps = len(op_names) // 2
        self._concat = concat
        self._indices = indices

        offset = 0
        tmp = 2
        for name, index in zip(op_names, indices):
            self._ops.append(OPERATIONS_MAP_2D[name](
                num_filters=self.num_filters,
                stride=2 if self.is_cell_reduction and index < 2 else 1,
                name=self.name + '_' + str(offset + index)
            ))
            offset += tmp
            tmp += 1

    def call(self, inputs, *args, **kwargs):
        input_0, input_1 = inputs
        state_in_0 = self._preprocessing_module_in_0(input_0)
        state_in_1 = self._preprocessing_module_in_1(input_1)

        states = [state_in_0, state_in_1]
        for i in range(self._steps):
            h_1 = self._ops[2 * i](states[self._indices[2 * i]])
            h_2 = self._ops[2 * i + 1](states[self._indices[2 * i + 1]])
            s = tf.add_n([h_1, h_2])
            states.append(s)

        return tf.concat([states[i] for i in self._concat], axis=-1)


class Cell(BaseCell):
    def __init__(self, num_nodes, num_reduction_nodes, num_filters, is_cell_reduction, is_prev_cell_reduction,
                 name=None, **kwargs):
        self.num_nodes = num_nodes
        self.num_reduction_nodes = num_reduction_nodes
        super(Cell, self).__init__(
            num_filters=num_filters,
            is_cell_reduction=is_cell_reduction,
            is_prev_cell_reduction=is_prev_cell_reduction,
            name=name,
            **kwargs)

    def _init_main_ops(self):
        self._ops = []
        offset = 0
        tmp = 2
        for i in range(self.num_nodes):
            for j in range(2 + i):
                self._ops.append(MixedOp(
                    num_filters=self.num_filters,
                    stride=2 if self.is_cell_reduction and j < 2 else 1,
                    name=self.name + '_' + str(offset + j)
                ))
            offset += tmp
            tmp += 1

    def call(self, inputs, *args, **kwargs):
        input_0, input_1, alphas_weights = inputs
        state_in_0 = self._preprocessing_module_in_0(input_0)
        state_in_1 = self._preprocessing_module_in_1(input_1)

        offset = 0
        states = [state_in_0, state_in_1]
        for i in range(self.num_nodes):
            s = tf.add_n([self._ops[offset + j]([h, alphas_weights[offset + j]]) for j, h in enumerate(states)])
            offset += len(states)
            states.append(s)

        return tf.concat(states[-self.num_reduction_nodes:], axis=-1)
